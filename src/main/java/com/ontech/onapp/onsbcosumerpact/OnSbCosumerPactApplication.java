package com.ontech.onapp.onsbcosumerpact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnSbCosumerPactApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnSbCosumerPactApplication.class, args);
	}

}
