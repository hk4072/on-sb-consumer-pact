package com.ontech.onapp.onsbcosumerpact.promos.requests;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Builder
@Getter
public class UpdateMobileNumberInPromoRequest {

    private BigDecimal userId;
    private String mobileNumber;
    private BigDecimal oldUserId;
}
