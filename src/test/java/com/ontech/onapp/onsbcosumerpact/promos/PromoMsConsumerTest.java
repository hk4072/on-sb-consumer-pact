package com.ontech.onapp.onsbcosumerpact.promos;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.dsl.DslPart;
import au.com.dius.pact.consumer.dsl.PactDslJsonBody;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.model.RequestResponsePact;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ontech.onapp.onsbcosumerpact.promos.requests.DeactivateUserBinPromoRequest;
import com.ontech.onapp.onsbcosumerpact.promos.requests.UpdateMobileNumberInPromoRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@ExtendWith(PactConsumerTestExt.class)
public class PromoMsConsumerTest {

    private static final String USER_ACTIVE_PROMO_ENDPOINT_URL = "/api/promos/active/1/false";

    private static final String IS_BIN_PROMO_ENDPOINT_URL = "/api/promos/1/has-bin-promo";

    private static final String DEACTIVATE_BIN_PROMO_ENDPOINT_URL = "/api/promos/deactivate-bin-promo";

    private static final String UPDATE_MOBILE_NUMBER_IN_PROMO_ENDPOINT_URL = "/api/promos/update-mobile-number";


    @BeforeEach
    public void setUp(MockServer mockServer) {
        Assertions.assertTrue(mockServer != null);
    }


    @Pact(state = "user active promos" , provider = "promoms-provider", consumer="promoms-consumer")
    public RequestResponsePact getUserActivePromoPact(PactDslWithProvider builder) {

        Map headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
                return builder
                .given("User has active Promos")
                .uponReceiving("valid user id and isAppRequest true/false ")
                .path(USER_ACTIVE_PROMO_ENDPOINT_URL)
                .method("GET")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body("{\n" +
                        "\"1\": {\n" +
                        "\"quantity\": 1,\n" +
                        "\"totalQuantity\": 1,\n" +
                        "\"promoValue\": \"AED 1\",\n" +
                        "\"description\": \"description\",\n" +
                        "\"endAt\": \"\",\n" +
                        "\"endAtMobile\": \"\",\n" +
                        "\"applicableFor\": 2,\n" +
                        "\"isExpired\": false,\n" +
                        "\"promoValue_ar\": \"إ.د 1\",\n" +
                        "\"description_ar\": \"\",\n" +
                        "\"endAt_ar\": \"\",\n" +
                        "\"endAtMobile_ar\": \"\"\n" +
                        "}\n" +
                        "}")
                .toPact();
    }

    @Pact(state = "user has bin promo" , provider = "promoms-provider", consumer="promoms-consumer")
    public RequestResponsePact getIsUserHasBinPromo(PactDslWithProvider builder) {

        Map headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        return builder
                .given("User has bin Promo")
                .uponReceiving("valid user id")
                .path(IS_BIN_PROMO_ENDPOINT_URL)
                .method("GET")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body("true")
                .toPact();
    }

    @Pact(state = "Deactivate bin promo" , provider = "promoms-provider", consumer="promoms-consumer")
    public RequestResponsePact deactivateBinPromo(PactDslWithProvider builder) {

        Map headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        return builder
                .given("User already has a bin promo")
                .uponReceiving("valid user id")
                .path(DEACTIVATE_BIN_PROMO_ENDPOINT_URL)
                .body(new PactDslJsonBody().integerType("userId").integerType("cardBin"))
                .method("POST")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body(getGeneralApiResponseBody())
                .toPact();
    }

    @Pact(state = "Update Mobile Number In Promo Request", provider = "promoms-provider", consumer = "promoms-consumer")
    public RequestResponsePact updateMobileNumberInPromo(PactDslWithProvider builder) {

        Map headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        return builder
                .given("User already have a verified mobile number")
                .uponReceiving("Valid Mobile Number, current userId and old userId")
                .path(UPDATE_MOBILE_NUMBER_IN_PROMO_ENDPOINT_URL)
                .body(new PactDslJsonBody()
                        .integerType("userId")
                        .integerType("oldUserId")
                        .stringType("mobileNumber"))
                .method("POST")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body(getGeneralApiResponseBody())
                .toPact();
    }

    private DslPart getGeneralApiResponseBody() {
        return new PactDslJsonBody().integerType("status").stringType("message").object("body");
    }

    @Test
    @PactTestFor(pactMethod = "getUserActivePromoPact")
    @Order(1)

    void shouldGetActiveUserPromosForGiveURL(MockServer mockServer) throws IOException, URISyntaxException {
        URI uri = new URIBuilder(mockServer.getUrl() + USER_ACTIVE_PROMO_ENDPOINT_URL).build();
        HttpResponse httpResponse = Request.Get(uri)
                .execute().returnResponse();
        Assertions.assertTrue(httpResponse.getStatusLine().getStatusCode() == 200);
    }

    @Test
    @PactTestFor(pactMethod = "getIsUserHasBinPromo")
    @Order(2)
    void shouldGetUserBinPromoForGiveURL(MockServer mockServer) throws IOException, URISyntaxException {
        URI uri = new URIBuilder(mockServer.getUrl() + IS_BIN_PROMO_ENDPOINT_URL).build();
        HttpResponse httpResponse = Request.Get(uri)
                .execute().returnResponse();
        Assertions.assertTrue(httpResponse.getStatusLine().getStatusCode() == 200);
    }

    @Test
    @PactTestFor(pactMethod = "deactivateBinPromo")
    @Order(3)
    void shouldDeactivateBinPromoForGiveURL(MockServer mockServer) throws IOException, URISyntaxException {
        URI uri = new URIBuilder(mockServer.getUrl() + DEACTIVATE_BIN_PROMO_ENDPOINT_URL).build();
        HttpResponse httpResponse = Request.Post(uri).bodyString(getDeactivateBinPromoBody(), ContentType.APPLICATION_JSON)
                .execute().returnResponse();
        Assertions.assertTrue(httpResponse.getStatusLine().getStatusCode() == 200);
    }

    private String getDeactivateBinPromoBody() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(DeactivateUserBinPromoRequest.builder()
                    .userId(BigDecimal.valueOf(100l))
                    .cardBin(BigDecimal.valueOf(100l)).build());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Test
    @PactTestFor(pactMethod = "updateMobileNumberInPromo")
    @Order(3)
    void shouldUpdateMobileNumberInPromoForGiveURL(MockServer mockServer) throws IOException, URISyntaxException {
        URI uri = new URIBuilder(mockServer.getUrl() + UPDATE_MOBILE_NUMBER_IN_PROMO_ENDPOINT_URL).build();
        HttpResponse httpResponse = Request.Post(uri).bodyString(getUpdateMobileNumberInPromoRequest(),
                ContentType.APPLICATION_JSON)
                .execute().returnResponse();
        Assertions.assertTrue(httpResponse.getStatusLine().getStatusCode() == 200);
    }

    private String getUpdateMobileNumberInPromoRequest() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(UpdateMobileNumberInPromoRequest.builder()
                    .userId(BigDecimal.valueOf(100l))
                    .oldUserId(BigDecimal.valueOf(100l))
                    .mobileNumber("string").build());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
