package com.ontech.onapp.onsbcosumerpact.promos.requests;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Builder
@Getter
@ToString
public class DeactivateUserBinPromoRequest {
    private BigDecimal userId;
    private BigDecimal cardBin;
}
