package com.ontech.onapp.onsbcosumerpact.orders;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.model.RequestResponsePact;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "orderms-provider", port = "8888")
public class OrderMsConsumerTest {

    private static final String USER_SUBSCRIPTION_URL = "/api/order/order/subscription?userId=2968&userSubscriptionId=51230";


    @BeforeEach
    public void setUp(MockServer mockServer) {
        Assertions.assertTrue(mockServer != null);
    }

    @Pact(state = "user order subscription" , provider = "orderms-provider", consumer="orderms-consumer")
    public RequestResponsePact orderUserSubscriptionPact(PactDslWithProvider builder) {
        Map headers = new HashMap<>();
        headers.put("Content-Type", "application/json;charset=UTF-8");
        return builder
                .given("Order User Subscription")
                .uponReceiving("user id and subscription id")
                .path("/api/order/order/subscription")
                .method("GET")
                .query("userId=2968&userSubscriptionId=51230")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body("true")
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "orderUserSubscriptionPact")
    void shouldPassResponseRecivedForGivenUserIdAndSubscriptionId(MockServer mockServer) throws IOException, URISyntaxException {
        URI uri = new URIBuilder(mockServer.getUrl() + USER_SUBSCRIPTION_URL)
                .setParameter("userId", "2968")
                .setParameter("userSubscriptionId", "51230").build();
        HttpResponse httpResponse = Request.Get(uri)
                .execute().returnResponse();
        Assertions.assertTrue(httpResponse.getStatusLine().getStatusCode() == 200);
    }
}
